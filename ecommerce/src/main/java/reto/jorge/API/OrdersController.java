package reto.jorge.API;

import reto.jorge.Model.Order;
import reto.jorge.Model.OrderProduct;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class OrdersController {
    protected static ArrayList<Order> ordersLists = new ArrayList<Order>() {
        {
            add(new Order("12/02/2020", "Jorge", 4));
            add(new Order("21/11/2020", "Ruben", 4));
            add(new Order("25/11/2020", "Diego", 1));
            add(new Order("05/09/2020", "Jaime", 2));
            add(new Order("19/10/2020", "Daniel", 3));
            add(new Order("19/10/2020", "Dabi", 3));
        }
    };

    protected static Order findOrderbyID(int id) {
        return ordersLists.get(id);
    }

    @GetMapping("/orders")
    public ArrayList<Order> getOrders() {
        return ordersLists;
    }

    @GetMapping("/orders/{id}")
    public Order getOrderbyId(@PathVariable int id) {
        return findOrderbyID(id);
    }

    /*
     * Devuelve los productos dentro del pedido {id}
     *
     */
    @GetMapping("/orders/{id}/products")
    public ArrayList<Object> getProductsInOrderbyId(@PathVariable int id) {
        ArrayList<Object> data = new ArrayList<>();
        // data.add(id);
        for (OrderProduct orderData : OrdersProductsController.ordersProductsLists) {
            if (orderData.getOrder().getId_order() == id) {
                data.add(orderData.getProduct());
            }
        }
        return data;
    }

    /*
     * Añade un nuevo pedido
     *
     */
    @PostMapping("/orders/products/{qty}")
    public int postMethodName( @PathVariable int qty, @RequestBody Order order) {
       
        Order orderAux = new Order();

        orderAux.setFe_creacion(order.getFe_creacion());
        orderAux.setUser(order.getUser());
        orderAux.setStatus(0);

        return orderAux.getId_order();
    }

    /*
    *   Actualiza un order
    */
    @PutMapping("/orders/{id}")
    public int putProd(@PathVariable int id, @RequestBody Order newOrder){
        for (Order order : ordersLists) {
            if(order.getId_order() == id){
                order.setUser(newOrder.getUser());
                order.setFe_creacion(newOrder.getFe_creacion());
                return 1;
            }
        }
        return 0;
    }


    /*
    *   Elimina order por {id}, también borra dependencias orderProduct
    */
    @DeleteMapping("/orders/{id}")
    public int deleteOrder(@PathVariable int id) {
        if (ordersLists.removeIf(o -> (o.getId_order() == id))) {
            OrdersProductsController.ordersProductsLists.removeIf(o -> ((o.getOrder().getId_order()) == id));
            return 1;
        }
        return 0;
    }

}
