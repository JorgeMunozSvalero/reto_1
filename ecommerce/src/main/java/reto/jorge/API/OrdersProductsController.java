package reto.jorge.API;

import java.util.ArrayList;

import reto.jorge.Model.Order;
import reto.jorge.Model.Product;
import reto.jorge.Model.OrderProduct;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class OrdersProductsController {
    protected static ArrayList<OrderProduct> ordersProductsLists = new ArrayList<OrderProduct>() {
        {
            add(new OrderProduct(1, ProductsController.productsList.get(0), OrdersController.ordersLists.get(1)));
            add(new OrderProduct(3, ProductsController.productsList.get(2), OrdersController.ordersLists.get(2)));
            add(new OrderProduct(1, ProductsController.productsList.get(3), OrdersController.ordersLists.get(3)));
            add(new OrderProduct(2, ProductsController.productsList.get(1), OrdersController.ordersLists.get(3)));
            add(new OrderProduct(1, ProductsController.productsList.get(3), OrdersController.ordersLists.get(0)));

        }
    };

    public static ArrayList<OrderProduct> findOrderProductbyIdOrder(int id){
        ArrayList<OrderProduct> list = new ArrayList<>();
        boolean found = false;
        for (OrderProduct object : ordersProductsLists) {
            if (object.getOrder().getId_order() == id) {
                list.add(object);
                found = true;
            }
        }
        if (found) {
            return list;
        }
        return list;
    }

    public static Double calculateTotalPrice(int id_order){
        ArrayList<OrderProduct> list_aux = findOrderProductbyIdOrder(id_order);
        double sum = 0;
        for (OrderProduct orderProduct : list_aux) {
            sum += (orderProduct.getProduct().getPrice() * orderProduct.getQuantity());
        }
        return sum;
    }

    private OrderProduct findbyIdOrderProduct(int id) {
        for (OrderProduct orderProduct : ordersProductsLists) {
            if (id == orderProduct.getId_orderproduct()) {
                return orderProduct;
            }
        }
        return null;
    }

    /*
    *   Recupera todos los orderProduct
    */
    @GetMapping("/orderproducts")
    public ArrayList<OrderProduct> getAllOrderProducts() {
        return ordersProductsLists;
    }

    /*
     *  Recupera todos los orderProduct
     *  correspondientes a orderProducto{id}
     */
    @GetMapping("/productsinorder/{id}")
    public ArrayList<OrderProduct> getProductsOrdersbyId(@PathVariable int id) {
       return findOrderProductbyIdOrder(id);
    
    }

    /*
     * Recupera todos los orderProduct
     * correspondientes a order{id}
     */
    @GetMapping("/orderproducts/order/{id}")
    public ArrayList<OrderProduct> getProductOrdersbyOrderId(@PathVariable int id) {
        ArrayList<OrderProduct> list = new ArrayList<>();
        boolean found = false;
        for (OrderProduct object : ordersProductsLists) {
            if (object.getOrder().getId_order() == id) {
                list.add(object);
                found = true;
            }
        }
        if (found) {
            return list;
        }
        return list;

    }

    /*
    *   Añadir producto al carrito/order. Si ya está añadido, no se duplica, se aumenta cantidad.
    */
    @PostMapping("/oders/{id}/product")
    public int postNewOP(@PathVariable int id, @RequestBody Product newProduct){

        for (OrderProduct orderProduct : findOrderProductbyIdOrder(id)) {
            if(orderProduct.getProduct().getId_product() == newProduct.getId_product()){
                orderProduct.setQuantity(orderProduct.getQuantity()+1);
                return 0;
            }
        }

        OrderProduct e = new OrderProduct();
        for (Order order : OrdersController.ordersLists) {
            if(order.getId_order() == id){
                e.setQuantity(1);
                e.setProduct(newProduct);
                e.setOrder(order);
                break;
            }
        }
        
        ordersProductsLists.add(e);
        return 1;
    }


    /* 
    *   Añadir/quitar cantidad {qty}(1, determinado en frontal)
    *   del producto del Body en el request, para el pedido {id}
    */
    @PutMapping(value = "orders/{id}/products/{qty}")
    public OrderProduct putOrderProduct(@PathVariable int id, @PathVariable int qty, @RequestBody Product newProduct, @RequestParam(name = "action", required = false, defaultValue = "add") String action) {
     
        boolean noStock = false;
        boolean exists = false;
        
        for (OrderProduct ordersProd : ordersProductsLists) {
            if(ordersProd.getOrder().getId_order() != id){
                exists = true;
            }
           
            if (ordersProd.getProduct().getId_product() == newProduct.getId_product()) {
                switch (action) {
                    case "add":
                        ordersProd.setQuantity((ordersProd.getQuantity() + qty));
                        exists = true;
                        break;
                    case "delete":
                        if (ordersProd.getQuantity() - qty <= 0) {
                            noStock = true;
                            break;
                        }
                        ordersProd.setQuantity((ordersProd.getQuantity() - qty));
                        break;
                    default:
                        break;
                }
                if (noStock) {
                    ordersProductsLists.remove(ordersProd);
                    return ordersProd;
                }
            }
            
             exists = false;
        }
        if(!exists){
           
        }
        return null;
        
    }


    /*
    *   Elimina relacion pedido-producto para orderProduct{id},
    *   corresponde a eliminar toda cantidad de un producto en la cesta
    */
    @DeleteMapping("/orderproducts/{id}")
    public void DeleteOP(@PathVariable("id") int id) {
        ordersProductsLists.remove(findbyIdOrderProduct(id));
    }

    /*
    *   Elimina todas las relaciones pedido-producto para order{id},
    *   corresponde a eliminar toda la cesta
    */
    @DeleteMapping("/orderproducts/order/{id}")
    public ArrayList<OrderProduct> DeleteAllOPbyOrderId(@PathVariable("id") int id) {

        ordersProductsLists.removeIf(o ->(o.getOrder().getId_order() == id));
        return ordersProductsLists;
        
    }

}
