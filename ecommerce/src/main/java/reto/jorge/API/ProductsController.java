package reto.jorge.API;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import reto.jorge.Model.Product;

@RestController
public class ProductsController {
    protected static ArrayList<Product> productsList = new ArrayList<Product>() {
        {
            add(new Product("TV Samsung 4K 65\"",750.00, "https://images.samsung.com/is/image/samsung/es-fhd-t530-ue32t5305akxxc-frontblack-227502935?$720_576_PNG$"));
            add(new Product("iPhone 12 128GB", 1199.99 , "https://s03.s3c.es/imag/_v0/770x420/6/4/3/490x_Iphone-12.jpg"));
            add(new Product("Batidora 120W", 69.99, "https://www.ecestaticos.com/image/clipping/727b884a857f68dc997de11a8f911ea9/las-mejores-batidoras-de-mano-que-podras-comprar-para-tu-cocina.jpg"));
            add(new Product("Patinete eléctrico", 450.00, "https://sgfm.elcorteingles.es/SGFM/dctm/MEDIA03/201912/04/00189060008946____2__640x640.jpg"));
            add(new Product("HP Probook i10", 999.99, "https://ssl-product-images.www8-hp.com/digmedialib/prodimg/lowres/c06186236.png"));
            add(new Product("Altavoz JBL 16W", 29.99, "https://es.jbl.com/dw/image/v2/AAUJ_PRD/on/demandware.static/-/Sites-masterCatalog_Harman/default/dwc1832f61/JBL_Boombox_Black_Hero-1605x1605.png?sw=537&sfrm=PNG"));
        }
    };

    private Product findProductById(int id){
        for(Product p : productsList){
            if(p.getId_product() == id){
                return p;
            }
        }
        return null;
    }

    /*
    *   Recupera todos los productos
    */
    @GetMapping("/products")
    public ArrayList<Product> getProducts() {
        return productsList;
    }

    /*
    *   Recupera producto{id}
    */
    @GetMapping("/products/{id}")
    public Product getProductsById(@PathVariable int id) {
        return findProductById(id);
    }

    /*
    *   Añade producto
    */
    @PostMapping("/products/{id}")
    public int postProd(@RequestBody Product newProduct){
        productsList.add(newProduct);
        return 1;
    }

    /*
    *   Actualiza un producto
    */
    @PutMapping("/products/{id}")
    public int putProd(@PathVariable int id, @RequestBody Product newProduct){
        for (Product product : productsList) {
            if(product.getId_product() == id){
                product.setName(newProduct.getName());
                product.setPrice(newProduct.getPrice());
                product.setUrl_product(newProduct.getUrl_product());
                return 1;
            }
            
        }
        return 0;
    }

    /*
    *   Elimina order por {id}, también borra dependencias orderProduct
    */
    @DeleteMapping("/products/{id}")
    public int deleteOrder(@PathVariable int id) {
        if (productsList.removeIf(o -> (o.getId_product() == id))) {
            OrdersProductsController.ordersProductsLists.removeIf(o -> ((o.getProduct().getId_product()) == id));
            return 1;
        }
        return 0;
    }

}
