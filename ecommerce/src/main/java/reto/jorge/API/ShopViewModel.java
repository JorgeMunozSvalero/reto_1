package reto.jorge.API;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ShopViewModel {

    @GetMapping("/home")
    public ModelAndView loadDetail(){
        ModelAndView mv = new ModelAndView("home");
        mv.addObject("products", ProductsController.productsList);
        mv.addObject("ordersProduct", OrdersProductsController.findOrderProductbyIdOrder(3));
        mv.addObject("totalAmountPrice", OrdersProductsController.calculateTotalPrice(3));
        return mv;
    }

}
