package reto.jorge.Model;

public class Order {
    private static int order_count = 0;
    private int id_order;
    private String fe_creacion;
    private String user;
    private String status;
    private static String[] statusArray = { "Processing", "Acepted", "Shipped", "Delivered", "Canceled" };

    /**
     * 
     * @param fe_creacion
     * @param user
     * @param statusSelectorArray Key-Value: 0-Processing, 1-Acepted, 2-Shipped,
     *                            3-Delivered, 4-Canceled
     * 
     */
    public Order(String fe_creacion, String user, int statusSelectorArray) {
        setId_order(order_count);
        ++order_count;
        this.fe_creacion = fe_creacion;
        this.user = user;
        this.status = statusArray[statusSelectorArray];
    }

    public Order() {
        setId_order(order_count);
        ++order_count;
    }

    public String getFe_creacion() {
        return fe_creacion;
    }

    public void setFe_creacion(String fe_creacion) {
        this.fe_creacion = fe_creacion;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = statusArray[status];
    }

    public static int getOrder_count() {
        return order_count;
    }

    public static void setOrder_count(int order_count) {
        Order.order_count = order_count;
    }

    public int getId_order() {
        return id_order;
    }

    public void setId_order(int id_order) {
        this.id_order = id_order;
    }

}
