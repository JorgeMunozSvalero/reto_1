package reto.jorge.Model;

public class OrderProduct {
	private static int countId = 1;
	private int id_orderproduct;
    private int quantity;
    private Product product;
	private Order order;
	
	public OrderProduct(int quantity, Product product, Order order) {
		setId_orderproduct(countId);
		++countId;
		this.quantity = quantity;
		this.product = product;
		this.order = order;
	}
	public OrderProduct() {
		setId_orderproduct(countId);
		++countId;
	}

	// public OrderProduct(int quantity, int idProduct, int idOrder) {
	// 	setId_orderproduct(countId);
	// 	++countId;
	// 	this.quantity = quantity;
	// 	this.id_order = idOrder;
	// }

	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public static int getCountId() {
		return countId;
	}
	public static void setCountId(int countId) {
		OrderProduct.countId = countId;
	}
	public int getId_orderproduct() {
		return id_orderproduct;
	}
	public void setId_orderproduct(int id_orderproduct) {
		this.id_orderproduct = id_orderproduct;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	
}
