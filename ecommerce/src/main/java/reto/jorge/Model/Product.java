package reto.jorge.Model;

public class Product {
    private static int product_count = 0;
    private int id_product;
    private String name;
    private double price;
    private String url_product;

    public Product() {
    }

    public Product(String name, double price, String url_product) {
        setId_product(product_count);
        ++product_count;
        this.name = name;
        this.price = price;
        this.url_product = url_product;
    }

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUrl_product() {
        return url_product;
    }

    public void setUrl_product(String url_product) {
        this.url_product = url_product;
    }

}
